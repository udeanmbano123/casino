package com.tipstosecrrtoundresu.online.mobile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler

class Splash : Activity() {
    // This is the loading time of the splash screen
    private val splashTimeout:Long = 3000 // 1 sec
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity
           startActivity(Intent(this,MainActivity::class.java))

            // close this activity
            finish()
        }, splashTimeout)
    }
}