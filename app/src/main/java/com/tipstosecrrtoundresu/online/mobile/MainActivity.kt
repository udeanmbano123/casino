package com.tipstosecrrtoundresu.online.mobile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ProgressBar
import java.io.IOException
import java.net.HttpURLConnection


@Deprecated("")
class MainActivity : Activity() {
    lateinit var webView: WebView
    lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Casino"
        progressBar = findViewById(R.id.progressBar)
        progressBar.visibility = View.GONE
        webView = findViewById<WebView>(R.id.webView)
        val settings = webView.settings
        webView.webViewClient = WebViewClient()
        webView.clearCache(true)
        webView.setWebChromeClient(WebChromeClient())
        webView.setWebViewClient(WebViewClient())
        //webView.getSettings().setAppCacheEnabled(false)
        // For API level below 18 (This method was deprecated in API level 18)
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        webView.loadUrl("http://hopeyyey.xyz/landing.php?yhuh=top&9=lucky&9=yesto")




    }

    inner class WebViewClient : android.webkit.WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

           if (url.contains("hopeyyey.xyz"))
            {
                view.loadUrl(url);
            }
            else
            {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(i)
            }
            return true
        }
        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar.visibility = View.GONE
        }
    }
    override fun onBackPressed() {
        if (webView!!.canGoBack()) {
            try{
                webView!!.goBack()
            }catch (e: Exception){

            }

        } else {
            try{
                super.onBackPressed()
            }catch (e: Exception){

            }

        }
    }

}